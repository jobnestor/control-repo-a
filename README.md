This is the control repo used automatically by [IaC-Heat-A](https://gitlab.com/jobnestor/iac-heat-a) on deployment.

After initialization subsequent r10k deployments will cause the root ssh key to disappear in common.yaml
We circumvented this with the following script (launch.sh)

r10k deploy environment -pv;
echo "base_linux::root_ssh_key: $(cut -d ' ' -f 2 /root/.ssh/id_rsa.pub)" >> /etc/puppetlabs/code/environments/production/data/common.yaml

For testing of puppet runs we install following packages on manager:
apt install jq
apt install python-pip
pip install yq

Then we run this script to validate the run (test_run.sh):

for f in $(find /opt/puppetlabs/server/data/puppetserver/reports/* \
 -type f -cmin -5); do echo $f; cat $f | yq -y .metrics.events.values; done

For lin1 we also had an own script where the path ended in ../reports/lin1.node.consul/* 
as this was our main email server.