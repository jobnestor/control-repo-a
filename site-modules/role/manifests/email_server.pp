# Postfix email server with postfix as MTA, dovecot as MDA
# and Rouncube as MUA
class role::email_server {
  include ::profile::base_linux
  include ::profile::dns::client
  include ::profile::consul::client
  include ::profile::postfix::install
  include ::profile::dovecot::server
}
