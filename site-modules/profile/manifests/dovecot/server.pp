#Installs Dovecot as standalone service
class profile::dovecot::server {

class { 'dovecot':
  login_greeting => 'Welcome back!',
}

class { 'dovecot::userdb': }
class { 'dovecot::passdb': }
class { 'dovecot::auth': }
class { 'dovecot::auth::unixlistener':
  socket => '/var/spool/postfix/private/auth',
}

class { 'dovecot::imaplogin':}

dovecot::account { 'admin@lin1':
  password => 'adminpassword',
}

dovecot::account { 'test1@lin1':
  password => 'password1',
}

}
