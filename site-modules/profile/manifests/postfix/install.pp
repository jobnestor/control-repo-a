# Install postfix and configurate virtual mail server
class profile::postfix::install {

  class { 'postfix':
    mydomain               => 'lin1',
    myorigin               => '$mydomain',
    inetinterfaces         => 'all',
    mynetworks             => [ '192.168.180.0/24', '127.0.0.0/8' ],
    smtpd_sasl_auth_enable => true,
  }

  class { 'postfix::vmail':
    setup_dovecot => false,
  }

  class { 'postfix::vmail::sasl':
    smtpd_sasl_type => 'dovecot',
    smtpd_sasl_path => 'private/auth',
  }

  postfix::vmail::account { 'test1@lin1':
    accountname => 'test1',
    domain      => 'lin1',
    password    => 'password1',
  }

  postfix::vmail::account { 'admin@lin1':
    accountname => 'admin',
    domain      => 'lin1',
    password    => 'adminpassword',
  }

}
