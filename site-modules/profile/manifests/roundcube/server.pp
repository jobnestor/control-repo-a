# This class implements roundcube and its its dependencies to be implemented on lin0

class profile::roundcube::server  {
  include git

  class { '::composer':
    command_name => 'composer',
    target_dir   => '/usr/local/bin'
  }

  file { '/opt':
    ensure => directory,
  }

    file { [
      '/var/',
      '/var/cache/',
      '/var/cache/puppet',
      '/var/cache/puppet/archives',
      ]:
      ensure => directory,
    }

  class { 'apache':
    mpm_module => 'itk',
  }

  package {[
    'php-xml',
    'php-mbstring',
    'php7.2-gd',
    'php-intl',
    'php-zip',
    'php-pear',
    ]:
    ensure => 'present',
  }

  class { 'apache::mod::php': }

  apache::vhost { 'roundcubemail':
    port    => '80',
    docroot => '/var/www/roundcubemail',
  }

  class {'::mysql::server':
    root_password           => 'strongpassword',
    remove_default_accounts => true,
  }

  mysql::db { 'roundcubemail':
    user     => 'roundcube',
    password => 'roundcube',
    host     => 'localhost',
    grant    => ['ALL'],
  }

  class { 'roundcube':
    db_type     => 'mysql',
    db_name     => 'roundcubemail',
    db_host     => 'localhost',
    db_username => 'roundcube',
    db_password => 'roundcube',
    plugins     => [
      'emoticons',
      'markasjunk',
      'password',
    ],
  }
}
